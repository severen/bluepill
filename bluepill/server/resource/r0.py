# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Implementations of the endpoints in the Client-Server API, revision 0."""

import random

from flask import request
from flask_restful import Resource
from webargs.flaskparser import use_kwargs
from marshmallow import Schema, fields, validate
from marshmallow.exceptions import ValidationError

try:
    from json import JSONDecodeError
except ImportError:
    # Hack to support Python 2 and 3.
    JSONDecodeError = ValueError

from bluepill.server.resource import \
    HOMESERVER, users, get_error_message, gen_token, validate_userid

# User Data Resources

# TODO: Implement the POST methods for the Profile resources.
class Profile(Resource):
    """Resource for the /_matrix/client/r0/profile/<user> endpoint."""

    # Common Errors
    BAD_USERID = get_error_message('M_NOT_FOUND', 'Could not find user.'), 404
    UNKNOWN_USER = get_error_message('M_UNKNOWN', 'Bad user ID.'), 400

    def get(self, user):
        if user in users:
            user = users[user]
            return {
                'displayname': user.get('displayname'),
                'avatar_url': user.get('avatar_url'),
            }
        elif validate_userid(user):
            return Profile.BAD_USERID
        elif not validate_userid(user):
            return Profile.UNKNOWN_USER

    class AvatarUrl(Resource):
        """
        Resource for the /_matrix/client/r0/profile/<user>/avatar_url endpoint.
        """
        def get(self, user):
            if user in users:
                user = users[user]
                return {'avatar_url': user.get('avatar_url')}
            elif validate_userid(user):
                return Profile.BAD_USERID
            elif not validate_userid(user):
                return Profile.UNKNOWN_USER

    class DisplayName(Resource):
        """
        Resource for the /_matrix/client/r0/profile/<user>/displayname
        endpoint.
        """
        def get(self, user):
            if user in users:
                user = users[user]
                return {'displayname': user.get('displayname')}
            elif validate_userid(user):
                return Profile.BAD_USERID
            elif not validate_userid(user):
                return Profile.UNKNOWN_USER

class Register(Resource):
    """Resource for the /_matrix/client/r0/register endpoint."""
    class BodySchema(Schema):
        """Schema for the request body."""
        # Schema Options
        class Meta(object):
            strict = True

        username = fields.String()
        bind_email = fields.Bool()
        password = fields.String(required=True)

    # TODO: Make errors for incorrect arguments match the Matrix error format.
    class ArgsSchema(Schema):
        """Schema for the query arguments."""
        # Schema Options
        class Meta(object):
            strict = True

        kind = fields.Str(
            required=True,
            validate=validate.OneOf(['guest', 'user']),
        )

    @use_kwargs(ArgsSchema())
    def post(self, kind):
        # Decode the request body from raw bytes into a UTF-8 string.
        body_raw = request.get_data().decode('utf-8')

        # Check that the request body is valid JSON and conforms to the schema.
        try:
            (body, _) = Register.BodySchema().loads(body_raw)
        except JSONDecodeError:
            return get_error_message(
                'M_NOT_JSON',
                'Content not JSON.',
            ), 400
        except ValidationError as e:
            # TODO: Find a better way to check these errors.
            errors = e.messages
            if 'password' in errors:
                return get_error_message(
                    'M_BAD_JSON',
                    'The supplied JSON was missing the password field.',
                ), 400
            else:
                return get_error_message(
                    'M_BAD_JSON',
                    'The supplied JSON contained errors.',
                ), 400

        # Construct the user ID.
        if 'username' in body:
            user_id = '@%s:%s' % (body.get('username'), HOMESERVER)
        else:
            # Generate a random ID between 1 and 100000 if none was given.
            while True:
                user_id = '@%s:%s' % (random.randint(1, 100000), HOMESERVER)
                if user_id not in users:
                    break

        # Check if the user ID is already taken.
        if user_id in users:
            return get_error_message(
                'M_USER_IN_USE',
                'User ID already taken.',
            ), 400

        # Generate access and refresh tokens.
        access_token = gen_token()
        refresh_token = gen_token()

        # Create a new user entry.
        users[user_id] = {
            'displayname': None,
            'avatar_url': None,
            'password': body.get('password'),
            'access_tokens': [access_token],
            'refresh_tokens': [refresh_token],
        }

        return {
            'user_id': user_id,
            'access_token': access_token,
            'home_server': HOMESERVER,
            'refresh_token': refresh_token,
        }

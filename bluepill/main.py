#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2016 Severen Redwood <severen@shrike.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from argparse import ArgumentParser

from bluepill import server

def main():
    parser = ArgumentParser(description='A mock server for testing Matrix SDKs.')  # noqa
    parser.add_argument(
        '-p',
        '--port',
        default=8008,
        dest='port',
        type=int,
        help='set the port (default: 8008)',
    )
    parser.add_argument(
        '-d',
        '--debug',
        action='store_true',
        help='enable debug mode',
    )

    args = parser.parse_args()
    server.run(args.port, args.debug)

if __name__ == "__main__":
    main()
